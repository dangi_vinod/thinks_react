import React from 'react';
import { withRouter, Link } from 'react-router-dom';
import axios from 'axios';
import { Table } from 'react-bootstrap';


class InventoryDetails extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            data: null
        }
    }

    async componentDidMount(){
        let id = this.props.history.location.pathname.split('/')[2]
        var obj = "";
        await axios.get(`http://127.0.0.1:8000/inventory_details/${id}`)
		.then(res => {
            console.log()
            obj = res.data
		})
		.catch(err => {
			alert('something went wrong !')
		})
        this.setState({
            data: obj
        })

    }

    render(){
        let { data } = this.state
        return(
            <>
                <h3>Inventory Details</h3>
                <Table striped bordered hover>
                    <tbody>
                        <tr>
                            <td>Name</td>
                            <td>{data && data.name}</td>
                        </tr>
                        <tr></tr>
                        <tr>
                            <td>Description</td>
                            <td>{data && data.description}</td>
                        </tr>
                        <tr></tr>
                        <tr>
                            <td>Price</td>
                            <td>{data && data.price}</td>
                        </tr>
                    </tbody>
            </Table>
            <Link to="/" >Go Back</Link>
            </>
        )
    }
}

export default withRouter(InventoryDetails);
