import React, { } from 'react';
import axios from 'axios';
import { withRouter } from 'react-router-dom';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';

import ListTable from './listTable'

class CreateInventory extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			name: "",
			description: "",
			price: "",
			inventoryList:[]
		}
	}

	componentDidMount(){
		this.fatchData();
	}

	async fatchData(){
		var obj = ""
		await axios.get('http://127.0.0.1:8000/')
		.then(res => {
			obj = res.data
		})
		.catch(err => {
			alert('Something went wrong')
		})
		this.setState({
			inventoryList: obj
		})
	}

	async handleSubmit(event){
		event.preventDefault();
		const data = {
			name: this.state.name,
			description: this.state.description,
			price: this.state.price
		}
		await axios.post('http://127.0.0.1:8000/', data)
		.then(function (response) {
			console.log(response);
		})
		.catch(function (error) {
			console.log(error);
		});
		this.fatchData();
	}

	handleOnChange = (event) => {
		this.setState({
			[event.target.name]: event.target.value
		})
	}

	deleteFunc = async (ids) => {
		await axios.delete(`http://127.0.0.1:8000/inventory_details/${ids.id}`)
		.then(res => {
			console.log()
		})
		.catch(err => {
			alert('something went wrong !')
		})
		this.fatchData()
	}

	inventoryDetails(id){
		this.props.history.push(`details/${id}`);
	}

	render(){
		let { inventoryList } = this.state
		return(
			<Container>
				<Row>
					<Col md={{ span: 6, offset: 3 }}>
						<Form onSubmit={this.handleSubmit.bind(this)} >
							<Form.Group as={Row} controlId="formPlaintextEmail">
								<Form.Label column sm="2">
									Name
								</Form.Label>
								<Col sm="10">
									<Form.Control 
										type="text"
										name="name"
										placeholder="Invenroty Name"
										value={this.state.name}
										onChange={this.handleOnChange}
									/>
								</Col>
							</Form.Group>

							<Form.Group as={Row} controlId="formPlaintextDescription">
								<Form.Label column sm="2">
								Description
								</Form.Label>
								<Col sm="10">
									<Form.Control 
									type="Text" 
									name="description" 
									placeholder="Inventory description"
									value={this.state.description}
									onChange={this.handleOnChange}
								/>
								</Col>
							</Form.Group>

							<Form.Group as={Row} controlId="formPlaintextPrice">
								<Form.Label column sm="2">
								Price
								</Form.Label>
								<Col sm="10">
									<Form.Control 
									type="number" 
									name="price" 
									placeholder="Inventory price" 	
									value={this.state.price}
									onChange={this.handleOnChange}
								/>
								</Col>
							</Form.Group>

							<Button variant="primary" type="submit">
								Add Item
							</Button>
						</Form>
					</Col>
				</Row>
				<ListTable 
					inventoryList={inventoryList}
					deleteFunc={this.deleteFunc}
					inventoryDetails={this.inventoryDetails.bind(this)}
				/>
			</Container>
		)
	}
}

export default withRouter(CreateInventory)
