import React from 'react';
import { Table, Button } from 'react-bootstrap';

class ListTable extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        let { inventoryList, deleteFunc, inventoryDetails } = this.props;
        return(
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {inventoryList.map((list, index) => {
                        return(
                            <tr key={index} onClick={() => inventoryDetails(list.id)} >
                                <td>{list.name}</td>
                                <td>{list.description}</td>
                                <td>{list.price}</td>
                                <td onClick={() => deleteFunc(list)}>
                                    <Button variant="danger" type="button">
                                    remove
                                </Button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </Table>
        )
    }
}
export default ListTable
