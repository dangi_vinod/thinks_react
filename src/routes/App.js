import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import CreateInventory from '../components/inventory'
import InventoryDetails from '../components/inventory/details'
import 'bootstrap/dist/css/bootstrap.min.css';
import '../App.css';

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" >
          <CreateInventory />
        </Route>

        <Route path="/details/:id" >
          <InventoryDetails />
        </Route>

      </Switch>
    </Router>    
  );
}

export default App;
